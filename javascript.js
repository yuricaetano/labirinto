const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];

//Consideração do mapa
let vazioY = [];
let vazioX = [];

let inicioY = [];
let inicioX = [];

let fimY = [];
let fimX = [];

let barreiraY = [];
let barreiraX = [];

let movimentaX = [];
let movimentaY = [];


function trackMap(){    
    for (let eixoY = 0; eixoY < map.length; eixoY++){
        let tamanhoDasParedesLateral = 4;
        let tamanhoDasParedesAltura = 4.9;
        let calculoPixelTop = eixoY * tamanhoDasParedesAltura + 20;
        linha = map[eixoY];        
        for (let eixoX = 0; eixoX < linha.length; eixoX++){
            calculoPixelLeft = eixoX * tamanhoDasParedesLateral + 7;
            caractere = linha[eixoX];
            desenha(caractere, calculoPixelTop, calculoPixelLeft);
            
                if (caractere == " "){
                    vazioY.push(eixoY)   
                    vazioX.push(eixoX)}                                                                                            
                else if (caractere == "S"){
                    inicioY.push(eixoY)
                    inicioX.push(eixoX)}              
                else if (caractere == "F"){
                    fimY.push(eixoY)
                    fimX.push(eixoX)}         
                else if (caractere == "W"){
                    barreiraY.push(eixoY)
                    barreiraX.push(eixoX)}                
        }
    }
}
trackMap()

document.addEventListener('keydown', teclado);
let posicaoInicioX = parseInt(inicioX);
let posicaoInicioY = parseInt(inicioY);


//Recebe o Inicio S 
function teclado(){  
    let aprova = 0;
    const qualTecla = event.key;  
    //Altera o valor
    if (qualTecla === 'ArrowUp'){      
        posicaoInicioY = posicaoInicioY -1;
        //console.log(movimentoY)
    }   
    else if (qualTecla === 'ArrowDown'){
        posicaoInicioY = posicaoInicioY +1;
    } 
    else if (qualTecla === 'ArrowRight'){
        posicaoInicioX = posicaoInicioX + 1;
    }
    else if (qualTecla === 'ArrowLeft'){
        posicaoInicioX = posicaoInicioX -1;
    }            
    if (posicaoInicioX == fimX && posicaoInicioY == fimY){

        let paragrafo = document.createElement("p");


        paragrafo.style.display = 'flex'
        paragrafo.style.width = '15%';
        paragrafo.style.height = '30%';    
        paragrafo.style.position = 'absolute'
        paragrafo.style.top = '35%'
        paragrafo.style.left = '42.5%'
        paragrafo.style.color = 'white';
        paragrafo.style.backgroundColor = 'red';
        paragrafo.textContent = "Parabéns! Voce conseguiu.";
        paragrafo.style.zIndex = '5';
        paragrafo.style.alignItems ="center"
        paragrafo.style.justifyContent = "center"
        paragrafo.style.border = "solid black 2px"
        paragrafo.style.fontSize = "30px"
        paragrafo.style.textAlign = "center"
        paragrafo.style.fontWeight = 'bold'

        document.getElementById("venceu").appendChild(paragrafo);
         
    }


    for (let i = 0; i < vazioY.length; i++){
        confereY = vazioY[i];
        confereX = vazioX[i];
            if ((confereY == posicaoInicioY) && (confereX == posicaoInicioX)){
                aprova = 1;
                //console.log("Aqui pode")
            }
    }

    if (aprova == 0){
        //console.log("Aqui não")

    if (qualTecla === 'ArrowUp'){      
        posicaoInicioY = posicaoInicioY +1;
        //console.log(movimentoY)
    }   
    else if (qualTecla === 'ArrowDown'){
        posicaoInicioY = posicaoInicioY -1;
    } 
    else if (qualTecla === 'ArrowRight'){
        posicaoInicioX = posicaoInicioX -1;
    }
    else if (qualTecla === 'ArrowLeft'){
        posicaoInicioX = posicaoInicioX +1;
    }   

    }
    
    movimentaY.push(posicaoInicioY)
    movimentaX.push(posicaoInicioX)   
    movimentaCachorrinho()
}

    //console.log("Movimento jogador Y = " + y)
    //console.log("Movimento jogador X = " + x)

    //console.log("Espaços vazio Y = " + vazioY)
    //console.log("Espaços vazio X = " + vazioX)

function desenha(caractere, calculoPixelTop, calculoPixelLeft){
        let identificacao = "";

        if (caractere == 'W') {
        corDeFundo = 'blue';
        }
        else if (caractere == ' ') {
        corDeFundo = 'white';

        }
        else if (caractere == 'S') {
        corDeFundo = 'white';
        identificacao = "inicio"
        }
        else if (caractere == 'F') {
        corDeFundo = 'orange';
        }
    
        let newDiv = document.createElement("div");
        newDiv.style.width = '5%';
        newDiv.id = identificacao
        newDiv.style.height = '5%';    
        newDiv.style.position = 'absolute'
        newDiv.style.top = calculoPixelTop + '%'
        newDiv.style.left = calculoPixelLeft + '%'
        newDiv.style.color = corDeFundo;
        newDiv.style.backgroundColor = corDeFundo;
        document.getElementById("mapa").appendChild(newDiv);
    
}
desenha()

function movimentaCachorrinho(){

    let cachorrinho = document.getElementById("cachorrinho"); 
    razaoDeMovimentoX = parseInt(movimentaX.slice(-1, movimentaX.length))
    razaoDeMovimentoY = parseInt(movimentaY.slice(-1, movimentaY.length))

    let tamanhoDasParedesLateral = 4;
    let tamanhoDasParedesAltura = 4.9;
    let calculoPixelTop = razaoDeMovimentoY * tamanhoDasParedesAltura + 20;        
        calculoPixelLeft = razaoDeMovimentoX * tamanhoDasParedesLateral + 7;

    cachorrinho.style.top = calculoPixelTop + '%'
    cachorrinho.style.left = calculoPixelLeft + '%'
    cachorrinho.style.width = document.getElementById("inicio").style.width;
    cachorrinho.style.height = document.getElementById("inicio").style.height;

    cachorrinho.style.position = 'absolute';
    cachorrinho.style.zIndex = '2';
    cachorrinho.style.backgroundImage = "url('./ratinho.png');"
    cachorrinho.style.backgroundRepeat = "no-repeat"
}
  



